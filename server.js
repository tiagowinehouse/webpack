'use strict';

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath, // arquivo de configuração de saida
  hot: true, //para fazer o hot-loader de forma correta
  historyApiFallback: true,
  stats: {
    colors: true //para a saída do webpack ser colorida
  }
}).listen(3000, (err) => {
  return err
    ? console.log(err)
    : console.log('Servidor WebpackDevServer em http://localhost:3000');
})
