'use strict';

const path = require('path');
const webpack = require('webpack');

module.exports = {
  devtool: 'source-map', //ficar mais fácil para debuggar o erro
  entry: [
    'react-hot-loader/patch', //arquivo de dependencia do hot-loader
    'webpack-dev-server/client?http://localhost:3000', //servidor e porta do webpack-dev-server
    'webpack/hot/only-dev-server', //evita recarregar erros de sintaxe
    path.join(__dirname, 'src', 'index'), //configurando o arquivo de entrada
  ],
  output: {
    path: path.join(__dirname, 'dist'), //configurando o arquivo de saida
    filename: 'bundle.js', //dando um nome para o arquivo de saída
    publicPath: '/static/' //é o local aonde o webpack-dev-server vai ficar assistindo o arquivo de desenvolvimento, agora o arquivo gerado vai fica na pasta static/bundle.js em memoria
  },
  plugins: [new webpack.HotModuleReplacementPlugin()], //fazendo a entrada de plugins

  module: {
    // preLoaders: [   {     test: /\.js$/, //todo arquivo que termina com .js
    // exclude: /node_modules/, //vai ignorar a pasta node_modules     include:
    // path.join(__dirname, 'src'), //o arquivo aonde vão ser lido     loader:
    // 'standard'   } ],
    loaders: [
      {
        test: /\.js$/, //todo arquivo que termina com .js
        exclude: /node_modules/, //vai ignorar a pasta node_modules
        include: path.join(__dirname, 'src'), //o arquivo aonde vão ser lido
        loader: 'babel-loader'
      }
    ]
  }
}
